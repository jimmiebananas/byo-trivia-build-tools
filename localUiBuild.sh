# Remove old UI files
cd ../byo-trivia-ui
rm -rf dist/
cd ../byo-trivia
rm -rf src/main/resources/static

# Build the UI
cd ../byo-trivia-ui
npm run webpack

# Copy UI to server directories.
cp -r dist/ ../byo-trivia/src/main/resources/static
