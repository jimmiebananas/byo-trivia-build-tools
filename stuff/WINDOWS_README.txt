Welcome to the trivia game!

If there are breaking bugs, please let me know so I can fix them

***Pre-requisites***
--- To run the trivia game, you need the Java 8 runtime environment (JRE) installed on your machine.
	- To see if you already have Java 8 installed:
	1) Click the Windows "Start" menu
	2) In the search bar type "cmd" and open "cmd.exe", which is the command prompt
	3) In the command prompt window, type "java -version" and some text will appear on the prompt
		a) If the Java version is above "1.8", then you're good to go!
		b) If the Java version is below "1.8", or it says something like "unrecognized command: java" then you need to install the Java 8 JRE.
		   Installation of the JRE should only take a few moments and the download link is:
		   http://download.oracle.com/otn-pub/java/jdk/8u73-b02/jre-8u73-windows-x64.exe


*** RUnning the game ***
--- Once Java is installed, to run the game, all you have to do is double-click the file in this folder called "launchTriviaGame.bat"
--- Running the batch script will open a new command prompt window. Once that window is open, DO NOT CLOSE IT! Closing the window will stop the game.
--- Once the game is running, open a new browser window and navigate to "http://localhost:8080/game"
	1) This will take you to the game window, and an URL will display on screen (something like http://192.168.1.12/play or http://10.32.1.16:8080/play)
	2) There are 3 main screens
		a) /game - this shows the game as it's progressing. This is the screen an audience would look at
		b) /host - this is only accessible on the host machine running the game. You can't get to it on your phone, for example because it will say
                   something about being "forbidden" access. This is so players don't have access to the host controls
		c) /play - this is the player screen, where clues and the player buzzer will appear at certain times during the game.
	3) IMPORTANT NOTE - IN ORDER TO PARTICIPATE YOU MUST BE ON THE SAME NETWORK AS THE HOST MACHINE (the machine you're reading this on and launching the game from)

*** Stopping the game ***
--- Stopping the game is as simple as going back to the command prompt window that opened when you clicked "launcTriviaGame.bat" and entering "ctrl + C" on the keyboard.

*** Adding questions ***
--- The game automatically reads any ".csv" files that are located in the "questionFiles" directory. You'll notice there are already two files there: "dummyQuestions.csv" and 
    "triviaQuestions.csv" (this is all 187 previous questions) If you wish to add a question set, simply make a new csv file and place it
    in this folder and then restart the app (ctr + c to stop it and then click the launcTriviaGame.bat to stop it again)
--- Your questions will load in the order they appear in your CSV file. Notice also that you need the header row (see pre-packaged csv files)


ENJOY THE GAME AND PLEASE LET ME KNOW IF YOU RUN INTO ANY PROBLEMS.

- Jim

        