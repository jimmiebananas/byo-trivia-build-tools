Welcome to the trivia game!

If there are breaking bugs, please let me know so I can fix them

***Pre-requisites***
--- To run the trivia game, you need the Java 8 runtime environment (JRE) installed on your machine.
	- To see if you already have Java 8 installed:
	1) Press the "command + spacebar" keys to open Spotlight Search on the Mac
	2) In the search bar type "terminal" and open the "Terminal" application (also located in Applications/Utilities)
	3) In the terminal window, type "java -version" and some text will appear on the prompt
		a) If the Java version is above "1.8", then you're good to go!
		b) If the Java version is below "1.8", or it says something like "unrecognized command: java" then you need to install the Java 8 JRE.
		   Installation of the JRE should only take a few moments and the download link is:

		   MAC OSX DOWNLOAD LOCATION: http://download.oracle.com/otn-pub/java/jdk/8u73-b02/jre-8u73-macosx-x64.dmg


*** RUnning the game ***
--- Once Java is installed, to run the game, all you have to do is double-click the file in this folder called "OSX_launchTriviaGame.app"
--- Running the app will open a new terminal window. Once that window is open, DO NOT CLOSE IT! Closing the window will stop the game.
--- Once the game is running, open a new browser window and navigate to "http://localhost:8080/game"
	1) This will take you to the game window, and an URL will display on screen (something like http://192.168.1.12/play or http://10.32.1.16:8080/play)
	2) There are 3 main screens
		a) /game - this shows the game as it's progressing. This is the screen an audience would look at
		b) /host - this is only accessible on the host machine running the game. You can't get to it on your phone, for example because it will say
                   something about being "forbidden" access. This is so players don't have access to the host controls
		c) /play - this is the player screen, where clues and the player buzzer will appear at certain times during the game.
	3) IMPORTANT NOTE - IN ORDER TO PARTICIPATE YOU MUST BE ON THE SAME NETWORK AS THE HOST MACHINE (the machine you're reading this on and launching the game from)

*** Stopping the game ***
--- Stopping the game is as simple as going back to the terminal window that opened when you clicked "OSX_launchTriviaGame.app" and pressing "ctrl + C" on the keyboard, or "ctrl + q" which will quite the Terminal app. Or just click "Terminal -> Quit" from the menu bar at the top of the screen to close Terminal and stop the running the trivia game.

*** Adding questions ***
--- The game automatically reads any ".csv" files that are located in the "questionFiles" directory. You'll notice there are already two files there: "dummyQuestions.csv" and 
    "triviaQuestions.csv" (this is all 187 previous questions) If you wish to add a question set, simply make a new csv file and place it
    in this folder and then restart the app (ctr + c to stop it and then click the launcTriviaGame.bat to stop it again)
--- Your questions will load in the order they appear in your CSV file. Notice also that you need the header row (see pre-packaged csv files)


ENJOY THE GAME AND PLEASE LET ME KNOW IF YOU RUN INTO ANY PROBLEMS.

- Jim

        