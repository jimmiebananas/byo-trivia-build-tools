# Remove and re-create the build directories
rm -rf build
rm -rf deploy

mkdir build
mkdir deploy

# Go to build directory
cd build

# Clone server project
git clone https://bitbucket.org/jimmiebananas/byo-trivia.git

# Clone UI prorject
git clone https://bitbucket.org/jimmiebananas/byo-trivia-ui.git
cd byo-trivia
mkdir src/main/resources/static

# Build UI project
cd ../byo-trivia-ui
npm install
npm run webpack

# Move UI into server project
cp -r dist/ ../byo-trivia/src/main/resources/static

# Build server project
cd ../byo-trivia
mvn clean install -Dmaven.test.skip=true

# Copy completed jar to deploy folder
cp target/trivia-0.0.1-SNAPSHOT.jar ../../deploy/

# Copy all the additional files to the deploy folder
cd ../../
cp -r stuff/ deploy/

# Zip everything up
cd deploy
zip -r TriviaGame.zip .
